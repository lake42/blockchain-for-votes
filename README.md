# Blockchain for votes

Some regions in Belgium have totally abandonned electronic votes. In October 2018, I have been called to manually count the paper votes. This experience alone made me want to advocate for the return of electronic votes.

As new elections are coming in June 2024, it is a great time to reopen the debate about electronic voting and the technologies that might reenable it. When electronic votes have been abandonned, Blockchain and Distributed Ledger technologies were not as known as now. Public studies about alternatives for electronic votes did not explore these technologies as [the last one was conducted in 2007](https://www.poureva.be/IMG/pdf/bevoting-1_gb.pdf)

The whitepaper will focus on:
- Why manually counting votes is a difficult task despite it sounds simple, based on my own experience.
- The benefits of electornic votes versus the paper votes.
- The challenges to solve and potential requirements regarding an electronic voting solution.
- Why and How Blockchain can be the ground technology for electronic voting.
- The challenges a Blockchain based electronic voting solution regarding security, auditability & accessibility.

This whitepaper will be submitted for review to the [Blockchain4Belgium](https://www.blockchain4belgium.eu/) and [WalChain](https://walchain.be/) communities.

## Table of Contents

- [Why manually counting paper votes is a difficult task](why-manually-counting-paper-votes-is-a-difficult-task)
- [Status](#status)
- [Contribute](#contribute)
- [Credits](#credits)

## Why manually counting paper votes is a difficult task

For people who have never made the experience, manually counting votes looks like a simple task that one will perform quick and easy with a big smile upon its face. This idea is false. Let me first describe you a voting day on October 2018 in Liege, Wallonia, Belgium.

### Being picked to work during the voting day

In Belgium, voting is compulsory. Belgian citizens are randomly picked and invited to present themselves at the election day either to work in the voting office either from morning until the office closure early afternoon or to manually count votes from early afternoon until ... they have finished. Yes, there is not any deadline to finish your task. It is also worth to mention election day is on Sunday and that no rest will be granted on Monday in case you are working. As a compensation, [you will receive something between 12.5€ and 18.5€](https://references.lesoir.be/article/combien-gagne-un-assesseur/). Cattering (water, a sandwish and waffles) is included.

As [23000 other citizens in Wallonia](https://www.rtbf.be/article/les-assesseurs-se-defilent-quelles-sont-les-raisons-valables-d-un-refus-10030308), I have received a letter which said I have been picked to work on the election day and that I will have to count votes. Unless you have a very good reason for not being there, [not presenting yourself will result in a heavy fine](https://www.rtbf.be/article/les-assesseurs-se-defilent-quelles-sont-les-raisons-valables-d-un-refus-10030308). So be it, rendez-vous on the 14th of October 2018.

### 14th October 2018: Voting day & start of a long day

14th of October, voting day, I get up early to be there at the voting office opening. As the voting office is near where I live, I go on foot. Many people have had the same thought. After having waited some time, I finally vote. I go back home and get some rest before the second round on the afternoon.

I must present myself at 2pm in another voting office to count votes. This other voting office is near the center of Liege. As we are on Sunday, public transports are rarer. Given I want to be on time and that I do not know when I will finish, I take my car to go there. Future will prove me this choice was the best I could do. After 10-15 minutes, I am parked near the voting office and I present myself.  

We are more than the required number to form the team that will count votes. It is normal: More people than needed are picked in order to ensure there will be enough people. I volunteer to stay because I am curious about doing this experience. Other people can go back to their home as all teams have enough people.

### Let the count begin

Somewhen between 2.30pm and 3pm, we are first brefied by the team leader about the procedure and requirements. We receive a bag full of votes. Here comes the easy part? Hell no!

One must understand that counting votes is harder than what it seems. We must ensure all votes have been taken into account. No mistake is tolerated, counts must match. There are also procedures to respect in opening and showing votes. For isntance, the team leader must open the vote, show it to one of the assistant and acknowledge whether the vote is valid or not. At any moment, a civil servant or a politic can show up in the room and watch what we do to ensure we do it right.

Here is what happened:
- First, we open the bag an pile votes unopened on a table. We all start counting unopened votes aat least TWICE to ensure we get the count of all votes in the bag.
- Once it is done, each vote is opened one by one. The team leader and an assitant first acknowledge whether the vote is valid or not. This take time because some case were difficult to assess. Voting is important, you do not want to discard a vote without a good reason. Several dozens of votes had to be discarded for the following reasons:
    - Drawings, insults, etc... Some were funny but after a dozen of male genitals, you get tired of it.
    - People not checking any boxes of their vote. Do not get me wrong: some people really did not understand they had to check the box. As instead, they put their mark somewhere next to the name of the list/candidate they support or round their names. 
- After having acknowledged the validity of the vote, votes are piled up on separated tables, one for each list.
- We count all the discarded votes and the count of votes for each list regardless the vote is for the list or for a candidate. We repeat the operation several times because from time to time we miss one vote and counts do not match with the total we had from the first step of counting.
- Once it is done, we count the votes for each list to determine how many votes were attributed for each candidates of the list. Once again, we have to repeat from time to time because of errors.

At the end of the day, each vote has been counted/manipulated at least five times: two times for the initital count, one time for the validity check, one time for the total list count and a last time for the candidate count. This is the very least because, as I said, we had to repeat many times because of errors. There were approximatively 1400 votes to count.

Let's do some maths: If it take 15 seconds on average to count/manipulate a vote, with 1400 votes processed at least five times each, you have 15 seconds * 1400 * 5 = 105000 seconds or almost 30 hours of work to do with some steps that can be done in parallel. As we are not machines, we have some breaks. 

Somewhen between 10pm and 11pm, we finally finish our task. I think we are among the first ones to have finished. No need to say there are not any buses on a Sunday at this hour. Fortunately, I have my car so I could drive back some other people and also drive back the team leader, the votes and the count to the "Palais de Justice" in Liege.

Past 11pm, I get back home. I am happy this voting day is over. This experience made me think a lot. I have met nice people and I am thankful for their attitude. Now, I have a good reason to advocate for electronic votes: I think in 2023, we have enough knowledge and technologies to envision better solutions that will not require to mobilize thousands of citizens a whole Sunday doing a daunting task until very late in the night for far less than the minimal salary. Especially that many of those citizens were woman with childs, unemployed people who we want to teach democracy and value of work...

## Status

- Drafting

## Contribute

Contribution guidelines will be released once the initial draft will be released by the author. Until then, no issues, pull requests or any sollicitations will be treated.

## Credits

- Project icon: <a href="https://www.flaticon.com/free-icons/election" title="election icons">Election icons created by Freepik - Flaticon</a>